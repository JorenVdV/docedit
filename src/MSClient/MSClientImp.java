package MSClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.apache.avro.AvroRemoteException;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.ipc.NettyServer;
import org.apache.avro.ipc.NettyTransceiver;
import org.apache.avro.ipc.Server;
import org.apache.avro.ipc.Transceiver;
import org.apache.avro.ipc.specific.SpecificRequestor;
import org.apache.avro.ipc.specific.SpecificResponder;
import org.jboss.netty.channel.DefaultChannelFuture;

import ds.project.client.gui.ClientGUI;
import ds.project.client.gui.ClientListener;
import MSdocedit.avro.MSClient;
import MSdocedit.avro.MSMaster;
import MSdocedit.avro.MSSlave;

public class MSClientImp implements MSClient, ClientListener {
	private MSMaster _proxy;
	private MSSlave _s_proxy;
	private static Server _server;
	private Transceiver _client;
	private Transceiver _slave;
	private ClientGUI _gui;
	private String _username;
	private String _document;
	private String _hostname;
	private String _m_hostname;
	private int _m_port;

	public static void main(String[] args) {
		int mport = 65111;
		String host = "localhost";
		String mhost = "localhost";
		if(args.length==3){
			host = args[0];
			mhost = args[1];
			mport = Integer.parseInt(args[2]);
		}
		MSClientImp cl = new MSClientImp(host, mhost, mport);
	}

	public MSClientImp(String host, String master_host, int master_port) {
		_hostname = host;
		_m_hostname = master_host;
		_m_port = master_port;
		start();
	}

	public void start() {
		_username = "";
		_document = "";
		_gui = ClientGUI.startGUI(this);
		try {
			_client = new NettyTransceiver(new InetSocketAddress(_m_hostname,
					_m_port));
			DefaultChannelFuture.setUseDeadLockChecker(false);
			_proxy = (MSMaster) SpecificRequestor.getClient(MSMaster.class,
					_client);
		} catch (IOException e) {
			_gui.showErrorMessage("Could not connect to server!");
			System.err.println("Error connecting to server...");
			e.printStackTrace(System.err);
			System.exit(1);
		}
		_gui.showLoginPane();
		System.out.println("Start done");
	}

	public void connectionLost() {
		_gui.showErrorMessage("Lost connection to Server!");
		try {
			_client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(1);
	}

	@Override
	public void exit() {
		if (!_username.isEmpty()) {
			try {
				_proxy.logout(_username);
			} catch (AvroRemoteException e) {
				e.printStackTrace();
				connectionLost();
			}
		}
		// server.close();
		try {
			_client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void login(String username, String password) {
		String hash = "";
		try {
			hash = DatatypeConverter.printHexBinary(MessageDigest.getInstance(
					"MD5").digest(password.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		try {
			int port = _proxy.login(username, hash, _hostname);
			if (port == 0) {
				_gui.showErrorMessage("User doesn't exist!");
			} else if (port == 1) {
				_gui.showErrorMessage("Wrong password!");
			} else if (port == 2) {
				_gui.showErrorMessage("User is already logged in!");
			} else if (port == 3) {
				_gui.showErrorMessage("There are no slaves available!");
			} else {
				this._username = username;
				_gui.showDocumentOverviewPane(_proxy.getDocumentList(username),
						username);
				_server = new NettyServer(new SpecificResponder(MSClient.class,
						this), new InetSocketAddress(_hostname, port));
			}
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		} catch (AvroRuntimeException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void register(String username, String password) {
		String hash = "";
		try {
			hash = DatatypeConverter.printHexBinary(MessageDigest.getInstance(
					"MD5").digest(password.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		if (username.isEmpty()) {
			_gui.showErrorMessage("You did not enter a user, try again");
		} else if (password.isEmpty()) {
			_gui.showErrorMessage("You did not enter a password, try again");
		} else {
			try {
				if (!_proxy.register(username, hash)) {
					_gui.showErrorMessage("Username already exist, try again");
				}
			} catch (AvroRemoteException e) {
				e.printStackTrace();
				connectionLost();
			}
		}
	}

	@Override
	public void logout() {
		try {
			_proxy.logout(_username);
			_username = "";
			_document = "";
			_server.close();
			_gui.showLoginPane();
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void openDocument(String documentName) {
		try {
			Map<CharSequence, Integer> map = _proxy.openDocument(_username,
					documentName);
			
			if(map==null){
				_gui.showErrorMessage("No slaves online");
				return;
			}
			
			if (map.size() == 1) {
				_document = documentName;
				Set<CharSequence> keySet = map.keySet();
				CharSequence slave_host = keySet.iterator().next();
				_slave = new NettyTransceiver(
						new InetSocketAddress(slave_host.toString(),
								map.get(slave_host)));
				_s_proxy = (MSSlave) SpecificRequestor.getClient(MSSlave.class,
						_slave);
				_gui.showDocumentEditorPane(documentName, _s_proxy
						.getDocumentTextByName(documentName).toString());
				_gui.setDocumentEditor(_s_proxy.getDocumentEditor(documentName)
						.toString());
			}
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void newDocument(String documentName) {
		try {
			_proxy.create(_username, documentName);
			updateAndShowDocumentList();
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void provideAccessRights(String document, String username) {
		try {
			_proxy.share(username, document);
			updateAndShowDocumentAccessRights(document);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void removeAccessRights(String document, List<String> usernames) {
		try {
			for (String user : usernames) {
				_proxy.unshare(user, document);
			}
			updateAndShowDocumentAccessRights(document);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void closeDocument() {
		try {
			_proxy.closeDocument(_username);
			_s_proxy.removeUserFromDocument(_username, _document);
			_s_proxy = null;
			_slave.close();
			_document = "";
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		} catch (IOException e) {
			e.printStackTrace();
		}
		updateAndShowDocumentList();
	}

	@Override
	public void editDocumentStart() {
		try {
			if (_s_proxy.startEditingDocument(_username)) {
				_gui.setDocumentEditable(true);
				_gui.setDocumentEditor(_username);
			} else {
				_gui.showInfoMessage("You can not edit at this moment, someone else is editing");
			}
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void editDocumentEnd() {
		try {
			_s_proxy.stopEditingDocument(_username);
			_gui.setDocumentEditable(false);
			_gui.setDocumentEditor("<NONE>");
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void insertText(String text, int position) {
		try {
			_s_proxy.editDocumentInsert(_username, text, position);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void deleteText(int start, int end) {
		try {
			_s_proxy.editDocumentDelete(_username, start, end);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void updateAndShowDocumentList() {
		try {
			_gui.showDocumentOverviewPane(_proxy.getDocumentList(_username),
					_username);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public void updateAndShowDocumentAccessRights(String document) {
		try {
			List<CharSequence> users = _proxy.getDocumentRights(document);
			String creator = users.get(0).toString();
			System.out.println(users);
			_gui.showDocumentAccessRightsPane(document, users, creator,
					creator.equals(_username));
		} catch (AvroRemoteException e) {
			e.printStackTrace();
			connectionLost();
		}
	}

	@Override
	public boolean UpdateDocument(CharSequence document)
			throws AvroRemoteException {
		_gui.showDocumentEditorPane(_document, document.toString());
		return false;
	}

	@Override
	public boolean UpdateEdit(CharSequence editor) throws AvroRemoteException {
		_gui.setDocumentEditor(editor.toString());
		return true;
	}

}
