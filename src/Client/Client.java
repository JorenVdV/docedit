package Client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.avro.AvroRemoteException;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.ipc.NettyTransceiver;
import org.apache.avro.ipc.NettyServer;
import org.apache.avro.ipc.Server;
import org.apache.avro.ipc.Transceiver;
import org.apache.avro.ipc.specific.SpecificRequestor;
import org.apache.avro.ipc.specific.SpecificResponder;

import docedit.avro.SharedDocs;
import docedit.avro.Users;
import ds.project.client.gui.ClientGUI;
import ds.project.client.gui.ClientListener;

public class Client implements Users, ClientListener {
	private SharedDocs proxy;
	private static Server server;
	private Transceiver client;
	private ClientGUI gui;
	private String username;
	private int num;

	public static void main(String[] args) {
		Client cl = new Client(65111);
		cl.start();
	}

	public Client(int num) {
		this.num = num;
	}

	public void start() {
		username = "";
		gui = ClientGUI.startGUI(this);
		try {
			client = new NettyTransceiver(new InetSocketAddress(num));
			proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class,
					client);
		} catch (IOException e) {
			gui.showErrorMessage("Could not connect to server!");
			System.err.println("Error connecting to server...");
			e.printStackTrace(System.err);
			System.exit(1);
		}
		gui.showLoginPane();
	}

	@Override
	public void exit() {
		try {
			if (!username.isEmpty()) {
				proxy.logout(username);
			}
			// server.close();
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<CharSequence> parseStrings(CharSequence s) {
		String[] sline = (s.toString()).split("\\$\\$\\$");
		ArrayList<CharSequence> lst = new ArrayList<CharSequence>();
		for (String str : sline) {
			if (!str.isEmpty()) {
				lst.add(str);
			}
		}
		return lst;
	}

	@Override
	public void login(String username, String password) {
		String hash = "";
		try {
			hash = DatatypeConverter.printHexBinary(MessageDigest.getInstance(
					"MD5").digest(password.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		try {
			int port = proxy.login(username, hash);
			if (port == 0) {
				gui.showErrorMessage("User doesn't exist!");
			} else if (port == 1) {
				gui.showErrorMessage("Wrong password!");
			} else if (port == 2) {
				gui.showErrorMessage("User is already logged in!");
			} else {
				this.username = username;
				gui.showDocumentOverviewPane(
						parseStrings(proxy.getDocs(username)), username);
				server = new NettyServer(new SpecificResponder(Users.class,
						this), new InetSocketAddress(port));
			}
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		} catch (AvroRuntimeException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void register(String username, String password) {
		String hash = "";
		try {
			hash = DatatypeConverter.printHexBinary(MessageDigest.getInstance(
					"MD5").digest(password.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		if (username.isEmpty()) {
			gui.showErrorMessage("You did not enter a user, try again");
		} else if (password.isEmpty()) {
			gui.showErrorMessage("You did not enter a password, try again");
		} else {
			try {
				if (!proxy.register(username, hash)) {
					gui.showErrorMessage("Username already exist, try again");
				}
				;
			} catch (AvroRemoteException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void logout() {
		try {
			// System.out.println("Pre logout");
			proxy.logout(username);
			// System.out.println("Post logout");
			username = "";
			server.close();
			// System.out.println("Post server shutdown");
			gui.showLoginPane();
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openDocument(String documentName) {
		try {
			gui.showDocumentEditorPane(documentName,
					proxy.view(this.username, documentName).toString());
			gui.setDocumentEditor(proxy.getEditor(documentName).toString());
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void newDocument(String documentName) {
		try {
			proxy.create(username, documentName);
			updateAndShowDocumentList();
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void provideAccessRights(String document, String username) {
		try {
			proxy.share(username, document);
			updateAndShowDocumentAccessRights(document);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeAccessRights(String document, List<String> usernames) {
		try {
			for (String user : usernames) {
				proxy.unshare(user, document);
			}
			updateAndShowDocumentAccessRights(document);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeDocument() {
		try {
			this.proxy.close(this.username);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
		updateAndShowDocumentList();
	}

	@Override
	public void editDocumentStart() {
		try {
			if (proxy.edit(username)) {
				gui.setDocumentEditable(true);
				gui.setDocumentEditor(username);
			}else{
				gui.showInfoMessage("You can not edit at this moment, someone else is editing");
			}
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void editDocumentEnd() {
		try {
			gui.setDocumentEditable(false);
			proxy.stopedit(this.username);

		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insertText(String text, int position) {
		try {
			proxy.insertText(username, text, position);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteText(int start, int end) {
		try {
			proxy.deleteText(username, start, end);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateAndShowDocumentList() {
		try {
			gui.showDocumentOverviewPane(parseStrings(proxy.getDocs(username)),
					username);
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateAndShowDocumentAccessRights(String document) {
		ArrayList<CharSequence> users = null;
		try {
			System.out.println("start");
			users = parseStrings(proxy.getDocInfo(document));
			String creator = users.get(0).toString();
			System.out.println(users);
			gui.showDocumentAccessRightsPane(document, users, creator,
					creator.equals(username));
		} catch (AvroRemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean UpdateDocuments() throws AvroRemoteException {
		gui.showDocumentOverviewPane(parseStrings(proxy.getDocs(username)),
				username);
		return true;
	}

	@Override
	public boolean UpdateDocument(CharSequence document)
			throws AvroRemoteException {
		gui.showDocumentEditorPane(document.toString(),
				proxy.view(this.username, document.toString()).toString());
		return false;
	}

	@Override
	public boolean UpdateEdit(CharSequence editor) throws AvroRemoteException {	
		gui.setDocumentEditor(editor.toString());
		return true;
	}

}

