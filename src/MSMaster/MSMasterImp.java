package MSMaster;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.avro.AvroRemoteException;
import org.apache.avro.ipc.NettyServer;
import org.apache.avro.ipc.NettyTransceiver;
import org.apache.avro.ipc.Server;
import org.apache.avro.ipc.Transceiver;
import org.apache.avro.ipc.specific.SpecificRequestor;
import org.apache.avro.ipc.specific.SpecificResponder;
import org.jboss.netty.channel.DefaultChannelFuture;

import MSdocedit.avro.MSMaster;
import MSdocedit.avro.MSSlave;

public class MSMasterImp extends TimerTask implements MSMaster {
	/*
	 * Master Server should keep track of: - registered users - logged in users
	 * (hostname+port) - document owners - document shares - should also
	 * distribute the necessary files to the slaves
	 */

	/*
	 * All variables
	 */
	private static Server _server;
	private static String _hostname;
	private static Integer _port;
	private static Timer _timer;

	// user related maps
	// list of all users and their passwords
	private static Map<String, String> _user_pwd; 
	// list of all users and their hostnames
	private static Map<String, String> _user_host;
	// list of all users and their port
	private static Map<String, Integer> _user_port;
	// list of documents viewed by an user
	private static Map<String, String> _user_open; 
	// list of all documents and their owners
	private static Map<String, String> _doc_owner; 
	// list of all documents and their shared to users
	private static Map<String, ArrayList<String>> _doc_shared; 
	 // used for backup purposes
	private static Map<String, String> _doc_content;

	// slave related maps
	// id give the functionality of having multiple slaves at the same address
	private int _slave_id = 0; 
	// contains all slaves by an id mapped to their IP
	private static Map<Integer, String> _slave_host;
	// contains all ports of the slaves
	private static Map<Integer, Integer> _slave_port; 
	// contains the dispersion of the documents
	private static Map<Integer, ArrayList<String>> _slave_doc; 

	public static void main(String[] args) {
		try {
			int mport = 65111;
			String mhost = "localhost";
			if(args.length==2){
				mhost = args[0];
				mport = Integer.parseInt(args[1]);
			}
			startServer(mhost, mport);

			Scanner reader = new Scanner(System.in);
			System.out.println("Enter the first number");
			reader.nextInt();
			reader.close();
			
			stopServer();
		} catch (IOException e) {
			_server.close();
			e.printStackTrace();
		}

	}

	public static void startServer(String mhost, int mport) throws IOException {
		// initialize all maps
		_user_pwd = new HashMap<String, String>();
		_user_host = new HashMap<String, String>();
		_user_port = new HashMap<String, Integer>();
		_user_open = new HashMap<String, String>();
		_doc_content = new HashMap<String, String>();

		_slave_doc = new HashMap<Integer, ArrayList<String>>();
		_slave_port = new HashMap<Integer, Integer>();
		_slave_host = new HashMap<Integer, String>();

		// read all input files on startup
		// read users and their passwords
		_user_pwd = get_map_from_file("Users/Users.txt");
		// read all owners
		_doc_owner = get_map_from_file("Users/Owners.txt");
		// read all shared users
		_doc_shared = get_multimap_from_file("Users/Shared.txt");

		// read all files in map Documents
		File folder = new File("Documents");
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				_doc_content.put(listOfFiles[i].getName(),
						get_file_to_string(listOfFiles[i].toString()));
			}
		}

		_timer = new Timer(); // create new timer
		_timer.schedule(new MSMasterImp(), 0, 30000); // schedule timer to write

		// set port and get hostname from the system
		_port = mport;
		_hostname = mhost;

		// startup server
		DefaultChannelFuture.setUseDeadLockChecker(false);
		_server = new NettyServer(new SpecificResponder(MSMaster.class,
				new MSMasterImp()), new InetSocketAddress(_port));

		System.out.println("Started Server");
	}

	public static void stopServer() throws IOException {
		// stop timer
		_timer.cancel();

		// write all necessary maps to files
		write_map_to_file("Users/Users.txt", _user_pwd);
		write_map_to_file("Users/Owners.txt", _doc_owner);
		write_multimap_to_file("Users/Shared.txt", _doc_shared);

		// request all documents from the slaves and write them locally
		try {
			for (Entry<Integer, ArrayList<String>> entry : _slave_doc
					.entrySet()) {
				Integer slave_id = entry.getKey();
				// construct proxy for slave

				Transceiver client = new NettyTransceiver(
						new InetSocketAddress(_slave_host.get(slave_id),
								_slave_port.get(slave_id)));
				// shutdown slave and get all documents back
				MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
						MSSlave.class, client);

				// add/ update all documents
				Map<CharSequence, CharSequence> temp = proxy.masterShutdown();
				for (Entry<CharSequence, CharSequence> entry2 : temp.entrySet()) {
					_doc_content.put(entry2.getKey().toString(), entry2
							.getValue().toString());
				}

			}
		} catch (IOException e) {
			_server.close();
			e.printStackTrace();
		}

		// write all documents
		for (Map.Entry<String, String> entry : _doc_content.entrySet()) {
			write_document("Documents/" + entry.getKey(), entry.getValue());
		}
		_server.close();
		System.out.println("Stopped Server");

	}

	@Override
	public int login(CharSequence user, CharSequence password,
			CharSequence hostname) throws AvroRemoteException {
		// check all preconditions
		// there must be slaves available
		if (!_user_pwd.containsKey(user.toString()))
			return 0; // error code for non-existing user
		else if (!_user_pwd.get(user.toString()).equals(password.toString()))
			return 1; // error code for wrong password
		else if (_user_host.containsKey(user.toString()))
			return 2; // user is already logged in
		// else if(_slave_host.isEmpty()) return 3; // errorcode for no slaves,
		// no working server
		else {
			// all preconditions met
			_port++;
			_user_host.put(user.toString(), hostname.toString());
			_user_port.put(user.toString(), _port);
			return _user_port.get(user.toString());
		}
	}

	@Override
	public boolean logout(CharSequence user) throws AvroRemoteException {
		// get server on which user is active if so
		System.out.println("got this far");
		if (_user_host.containsKey(user.toString()))
			_user_host.remove(user.toString());
		if (_user_port.containsKey(user.toString()))
			_user_port.remove(user.toString());
		if (_user_open.containsKey(user.toString())) {
			// user is viewing an document and hence linked to a server
			// get proxy to the server
			System.out.println("hope");
			
			Integer slave_id = -1;
			for (Entry<Integer, ArrayList<String>> entry : _slave_doc.entrySet()) {
				if (entry.getValue().contains(_user_open.get(user.toString()))) {
					slave_id = entry.getKey();
					break;
				}
			}
			if (slave_id ==-1) return false;

			// construct proxy for slave
			try {
				Transceiver client = new NettyTransceiver( new InetSocketAddress(_slave_host.get(slave_id),
								_slave_port.get(slave_id)));
				MSSlave proxy = (MSSlave) SpecificRequestor.getClient(MSSlave.class, client);
				
				proxy.logoutUser(user);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// after removing the user from the slaves remove him from master system
		if (_user_open.containsKey(user.toString()))
			_user_open.remove(user.toString());

		return true;
	}

	@Override
	public List<CharSequence> getDocumentList(CharSequence user)
			throws AvroRemoteException {
		ArrayList<CharSequence> docs = new ArrayList<CharSequence>();
		// check which docs the user owns
		for (Map.Entry<String, String> entry : _doc_owner.entrySet()) {
			if (entry.getValue().equals(user.toString()))
				docs.add(entry.getKey());
		}

		// check which docs the user has rights to
		Set<String> keySet = _doc_shared.keySet();
		Iterator<String> keyIterator = keySet.iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			ArrayList<String> values = _doc_shared.get(key);
			if (values.contains(user.toString()))
				docs.add(key);
		}

		return docs;
	}

	@Override
	public Map<java.lang.CharSequence, Integer> openDocument(CharSequence user,
			CharSequence document) throws AvroRemoteException {
		// gives the user the name and port of the slave
		// loop all slaves to find the document
		Integer slave_id = -1;
		Set<Integer> keySet = _slave_doc.keySet();
		Iterator<Integer> keyIterator = keySet.iterator();
		while (keyIterator.hasNext()) {
			Integer key = keyIterator.next();
			ArrayList<String> values = _slave_doc.get(key);
			if (values.contains(document.toString()))
				slave_id = key;
		}
		if (slave_id == -1)
			return null;

		_user_open.put(user.toString(), document.toString());

		Map<CharSequence, Integer> temp = new HashMap<CharSequence, Integer>();
		temp.put(_slave_host.get(slave_id), _slave_port.get(slave_id));
		// adds the user to the document on the slave
		// build proxy to slave
		try {
			Transceiver client = new NettyTransceiver(new InetSocketAddress(
					_slave_host.get(slave_id), _slave_port.get(slave_id)));
			MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
					MSSlave.class, client);
			proxy.addUserToDocument(user, document,
					_user_host.get(user.toString()),
					_user_port.get(user.toString()));
			client.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public boolean closeDocument(CharSequence user) throws AvroRemoteException {
		// removes user from the slave using removeUserFromDocument
		// find document name
		if (!_user_open.containsKey(user.toString()))
			return false;
		String document = _user_open.get(user.toString());

		// find slave
		Integer slave_id = 0;
		Set<Integer> keySet = _slave_doc.keySet();
		Iterator<Integer> keyIterator = keySet.iterator();
		while (keyIterator.hasNext()) {
			Integer key = keyIterator.next();
			ArrayList<String> values = _slave_doc.get(key);
			if (values.contains(document.toString()))
				slave_id = key;
		}
		if (slave_id == 0)
			return false;
		Map<CharSequence, Integer> temp = new HashMap<CharSequence, Integer>();
		temp.put(_slave_host.get(slave_id), _slave_port.get(slave_id));
		// removes the user to the document on the slave
		// build proxy to slave
		try {
			Transceiver client = new NettyTransceiver(new InetSocketAddress(
					_slave_host.get(slave_id), _slave_port.get(slave_id)));
			MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
					MSSlave.class, client);
			proxy.removeUserFromDocument(user, document);
			client.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public boolean create(CharSequence user, CharSequence document)
			throws AvroRemoteException {
		// create a new file and add user as owner
		try {
			File f = new File("Documents/" + document);
			f.createNewFile();
			_doc_owner.put(document.toString(), user.toString());
			// loop 2 times over slaves, minimum value should accept the new
			// document
			int minimum = Integer.MAX_VALUE;
			Integer key = 0;
			for (Map.Entry<Integer, ArrayList<String>> entry : _slave_doc
					.entrySet()) {
				if (entry.getValue().size() < minimum) {
					minimum = entry.getValue().size();
					key = entry.getKey();
				}
			}
			_slave_doc.get(key).add(document.toString());
			try {
				Transceiver client = new NettyTransceiver(
						new InetSocketAddress(_slave_host.get(key),
								_slave_port.get(key)));
				MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
						MSSlave.class, client);
				proxy.addDocument(document, "");
				client.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

			return true;
		} catch (IOException e) {

		}
		return false;
	}

	@Override
	public boolean share(CharSequence user, CharSequence document)
			throws AvroRemoteException {
		if (_doc_owner.containsKey(document.toString())
				&& _user_pwd.containsKey(user.toString())) {
			if (_doc_shared.containsKey(document.toString()))
				_doc_shared.get(document.toString()).add(user.toString());
			else {
				ArrayList<String> lst = new ArrayList<String>();
				lst.add(user.toString());
				_doc_shared.put(document.toString(), lst);
			}
			return true;

		}

		return false;
	}

	@Override
	public boolean unshare(CharSequence user, CharSequence document)
			throws AvroRemoteException {
		// check whether document and user exists
		if (_doc_owner.containsKey(document.toString())
				&& _user_pwd.containsKey(user.toString())) {
			if (_doc_shared.get(document.toString()).contains(user.toString())) {
				// delete user
				_doc_shared.get(document.toString()).remove(user.toString());
				return true;
			}

		}
		return false;
	}

	@Override
	public boolean register(CharSequence user, CharSequence password)
			throws AvroRemoteException {
		if (!_user_pwd.containsKey(user.toString())) {
			_user_pwd.put(user.toString(), password.toString());
			return true;
		}
		return false;
	}

	@Override
	public List<CharSequence> getDocumentRights(CharSequence document)
			throws AvroRemoteException {
		// returns only all user that can view/edit the document no owner
		// mentioned
		List<CharSequence> users = new ArrayList<CharSequence>();
		users.add(_doc_owner.get(document.toString()));

		if (_doc_shared.containsKey(document.toString())) {
			for (String entry : _doc_shared.get(document.toString())) {
				users.add(entry);
			}
		}

		return users;

	}

	@Override
	public boolean addSlave(CharSequence hostname, int port)
			throws AvroRemoteException {
		// get all files updated and removed from the slaves
		if (!_slave_doc.isEmpty()) {
			for (Map.Entry<Integer, ArrayList<String>> entry : _slave_doc
					.entrySet()) {
				// retrieve all documents from the slave
				for (String doc : entry.getValue()) {
					// build proxy to slave
					Transceiver client;
					try {
						DefaultChannelFuture.setUseDeadLockChecker(false);
						client = new NettyTransceiver(new InetSocketAddress(
								_slave_host.get(entry.getKey()),
								_slave_port.get(entry.getKey())));

						MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
								MSSlave.class, client);

						_doc_content.put(doc, proxy.deleteDocument(doc)
								.toString());
						client.close();

					} catch (IOException e) {
						e.printStackTrace();
						return false;
					}

				}
				entry.setValue(new ArrayList<String>()); // empty out the list
			}
		}

		// add slave to the slave maps
		_slave_host.put(_slave_id, hostname.toString());
		_slave_port.put(_slave_id, port);
		_slave_doc.put(_slave_id, new ArrayList<String>());

		// repush all the files to the slave servers
		// loop over documents
		Set<Integer> slave_ids = _slave_host.keySet();
		Object[] slave_id = slave_ids.toArray();
		int count = 0;
		int max = slave_ids.size();
		for (Map.Entry<String, String> entry : _doc_content.entrySet()) {
			// choose next server and build proxy to this server
			try {
				Transceiver client = new NettyTransceiver(
						new InetSocketAddress(_slave_host.get(slave_id[count]),
								_slave_port.get(slave_id[count])));
				MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
						MSSlave.class, client);
				proxy.addDocument(entry.getKey(), entry.getValue());
				client.close();

				_slave_doc.get(slave_id[count]).add(entry.getKey());
				count += 1;
				if (count >= max)
					count = 0;

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

		}

		return true;
	}

	@Override
	public boolean closeSlave(CharSequence hostname, int port)
			throws AvroRemoteException {
		
		//find slave id
		int slave=-1;
		if(_slave_host.isEmpty()) return false;
		for(Map.Entry<Integer, String> entry: _slave_host.entrySet()){
			if(entry.getValue().equals(hostname.toString()) &&
					_slave_port.get(entry.getKey()) == port){
				slave = entry.getKey();
			}

		}
		if (slave == -1)
			return false;

		// get all files updated and removed from the slaves
		for (Map.Entry<Integer, ArrayList<String>> entry : _slave_doc
				.entrySet()) {
			// retrieve all documents from the slave
			for (String doc : entry.getValue()) {
				// build proxy to slave
				Transceiver client;
				try {
					client = new NettyTransceiver(new InetSocketAddress(
							_slave_host.get(entry.getKey()),
							_slave_port.get(entry.getKey())));
					MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
							MSSlave.class, client);
					_doc_content.put(doc, proxy.deleteDocument(doc).toString());
					client.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}

			}
		}
		// delete slave from the slave maps
		_slave_host.remove(slave);
		_slave_port.remove(slave);
		_slave_doc.remove(slave);

		// repush all the files to the slave servers
		//loop over documents
		if(_slave_host.isEmpty())return true;

		Set<Integer> slave_ids = _slave_host.keySet();
		Object[] slave_id = slave_ids.toArray();
		int count = 0;
		int max = slave_ids.size();
		for (Map.Entry<String, String> entry : _doc_content.entrySet()) {
			// choose next server and build proxy to this server
			try {
				Transceiver client = new NettyTransceiver(
						new InetSocketAddress(_slave_host.get(slave_id[count]),
								_slave_port.get(slave_id[count])));
				MSSlave proxy = (MSSlave) SpecificRequestor.getClient(
						MSSlave.class, client);
				proxy.addDocument(entry.getKey(), entry.getValue());
				client.close();

				_slave_doc.get(slave_id[count]).add(entry.getKey());
				count += 1;
				if (count >= max)
					count = 0;

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

		}
		return true;
	}

	@Override
	public CharSequence getDocowner(CharSequence docname)
			throws AvroRemoteException {
		return _doc_owner.get(docname.toString());
	}

	@Override
	public Void slaveWriteBack(CharSequence document, CharSequence content)
			throws AvroRemoteException {
		if (_doc_content.containsKey(document.toString()))
			_doc_content.put(document.toString(), content.toString());
		return null;
	}

	@Override
	public void run() {
		// write all files to the system (copy the shutdown procedure)
		// will not pull newest version of files from the slaves sinds they will
		// send updates

		// write all necessary maps to files
		write_map_to_file("Users/Users.txt", _user_pwd);
		write_map_to_file("Users/Owners.txt", _doc_owner);
		write_multimap_to_file("Users/Shared.txt", _doc_shared);

		// write all documents
		for (Map.Entry<String, String> entry : _doc_content.entrySet()) {
			write_document("Documents/" + entry.getKey(), entry.getValue());
		}

	}

	/*
	 * ########################## Utility functions ###########################
	 */

	private static String get_file_to_string(String docname) {
		/*
		 * get_file_to_string(docname) gets the content from a file and puts
		 * this into a string newlines are replace by the java character "\n"
		 */
		String rv = "";

		try {
			// get file from the system
			File f = new File(docname);

			// construct a file reader for the file
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				rv += line;
				rv += "\n";

			}
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

		return rv;
	}

	private static boolean write_map_to_file(String docname,
			Map<String, String> map) {
		try {
			// get the file from the system, directory have to be handled above
			File f = new File(docname);

			// construct a file writer for the file
			PrintWriter pw = new PrintWriter(new FileWriter(f));
			pw.write("".toCharArray()); // empty out file

			int count = map.size();
			for (Map.Entry<String, String> entry : map.entrySet()) {
				// write line to file
				pw.append(entry.getKey() + "$$$" + entry.getValue());
				if (count > 1)
					pw.append("\n");
				count--;
			}

			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private static void write_multimap_to_file(String docname,
			Map<String, ArrayList<String>> doc_shared) {
		// if(doc_shared2.isEmpty())return;
		String towrite = "";

		Set<String> keySet = doc_shared.keySet();
		Iterator<String> keyIterator = keySet.iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			ArrayList<String> values = doc_shared.get(key);
			Iterator<String> valIterator = values.iterator();
			while (valIterator.hasNext()) {
				String val = valIterator.next();
				towrite += key + "$$$" + val + "\n";
			}
		}

		if (towrite.length() > 0)
			towrite = towrite.substring(0, towrite.length() - 1);

		write_document(docname, towrite);
	}

	private static void write_document(String docname, String contents) {
		try {
			File f = new File(docname);
			// if(!f.exists())f.createNewFile();
			PrintWriter pw = new PrintWriter(new FileWriter(f));
			pw.write(contents);
			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static HashMap<String, String> get_map_from_file(String docname) {

		try {
			HashMap<String, String> map = new HashMap<String, String>();

			File f = new File(docname);
			if (!f.exists()) {
				f.createNewFile();
				return map;
			}
			BufferedReader bv = new BufferedReader(new FileReader(f));

			String line;
			while ((line = bv.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				map.put(sline[0], sline[1]);
			}

			bv.close();
			return map;

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	private static HashMap<String, ArrayList<String>> get_multimap_from_file(
			String docname) {
		try {
			HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();

			File f = new File(docname);
			if (!f.exists()) {
				f.createNewFile();
				return map;
			}
			BufferedReader bv = new BufferedReader(new FileReader(f));

			String line;
			while ((line = bv.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (map.containsKey(sline[0]))
					map.get(sline[0]).add(sline[1]);
				else {
					ArrayList<String> lst = new ArrayList<String>();
					lst.add(sline[1]);
					map.put(sline[0], lst);
				}
			}

			bv.close();
			return map;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return null;
	}
}
