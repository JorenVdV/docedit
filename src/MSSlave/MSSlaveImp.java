package MSSlave;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.avro.AvroRemoteException;
import org.apache.avro.ipc.NettyServer;
import org.apache.avro.ipc.NettyTransceiver;
import org.apache.avro.ipc.Server;
import org.apache.avro.ipc.Transceiver;
import org.apache.avro.ipc.specific.SpecificRequestor;
import org.apache.avro.ipc.specific.SpecificResponder;
import org.jboss.netty.channel.DefaultChannelFuture;

import MSdocedit.avro.MSClient;
import MSdocedit.avro.MSMaster;
import MSdocedit.avro.MSSlave;

public class MSSlaveImp extends TimerTask implements MSSlave {
	private static Server _server;
	private static String _hostname;
	private static HashMap<String, String> _user_open;
	private static HashMap<String, String> _user_host;
	private static HashMap<String, Integer> _user_port;
	private static Map<String, String> _user_edit;
	private static Map<String, String> _doc_content;
	private static int _port;
	private static int _m_port;
	private static String _m_hostname;
	private static Timer _timer;
	private MSMaster _m_proxy;
	private NettyTransceiver _tranceiver;

	public static void main(String[] args) {
		int port = 65120;
		int mport = 65111;
		String host = "localhost";
		String mhost = "localhost";
		if(args.length==4){
			host = args[0];
			port = Integer.parseInt(args[1]);			
			mhost = args[2];
			mport = Integer.parseInt(args[3]);
		}		
		MSSlaveImp slave = new MSSlaveImp(host, port, mhost, mport);
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter the first number");
		reader.nextInt();
		reader.close();
		slave.stop();
	}

	public MSSlaveImp(String host, int port, String mhost, int mport) {
		_hostname = host;
		_m_hostname = mhost;
		_m_port = mport;
		_port = port;
		start();
	}

	public void start() {
		// initialize all maps
		_user_host = new HashMap<String, String>();
		_user_port = new HashMap<String, Integer>();
		_user_open = new HashMap<String, String>();
		_doc_content = new HashMap<String, String>();
		_user_edit = new HashMap<String, String>();
		_timer = new Timer(); // create new timer
		_timer.schedule(this, 0, 30000); // schedule timer to write
											// each 30 seconds
		// startup server
		DefaultChannelFuture.setUseDeadLockChecker(false);
		_server = new NettyServer(new SpecificResponder(MSSlave.class, this),
				new InetSocketAddress(_hostname, _port));

		System.out.println("Start");

		try {
			_tranceiver = new NettyTransceiver(new InetSocketAddress(
					_m_hostname, _m_port));
			_m_proxy = (MSMaster) SpecificRequestor.getClient(MSMaster.class,
					_tranceiver);
			_m_proxy.addSlave(_hostname, _port);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}

		System.out.println("Started Slave");
	}

	public void stop() {
		try {
			_m_proxy.closeSlave(_hostname, _port);
		} catch (AvroRemoteException e) {
			_server.close();
			e.printStackTrace();
		}
		_server.close();
		System.out.println("Slave stopped");
	}

	@Override
	public boolean addUserToDocument(CharSequence user, CharSequence document,
			CharSequence host, int port) throws AvroRemoteException {
		if (!_doc_content.containsKey(document.toString())) {
			System.out.println("Something went wrong");
			return false;
		}

		_user_port.put(user.toString(), port);
		_user_host.put(user.toString(), host.toString());
		_user_open.put(user.toString(), document.toString());
		return true;
	}

	@Override
	public boolean startEditingDocument(CharSequence user)
			throws AvroRemoteException {
		/*
		 * edit(user) puts a user inside the edit map uses the document from the
		 * view map if user is not in view map false will be returned and the
		 * user will not be granted access
		 */
		System.out.println("Slave.startEdit");
		if (!_user_open.containsKey(user.toString()))
			return false;
		else {
			String docname = _user_open.get(user.toString());
			System.out.println(docname);

			// check that no one else is writing in the document
			if (_user_edit.get(docname).equals("<NONE>".toString())) {
				// System.out.println("start");
				_user_edit.put(docname, user.toString());
				// update all viewers of new editor
				updateEditors(docname, user.toString());
				System.out.println(user.toString() + " is editing");
				return true;
			} else {
				return false;
			}
		}
	}

	public void updateEditors(String docname, String editor) {
		for (Map.Entry<String, String> entry : _user_open.entrySet()) {
			System.out.println(entry);
			if (entry.getValue().equals(docname)
					&& !entry.getKey().equals(editor)) {
				// skip if user is the editer
				try {
					Transceiver client = new NettyTransceiver(
							new InetSocketAddress(
									_user_host.get(entry.getKey()),
									_user_port.get(entry.getKey())));
					MSClient proxy = (MSClient) SpecificRequestor.getClient(
							MSClient.class, client);
					proxy.UpdateEdit(editor);
					client.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void updateViews(String docname, String editor, String content) {
		for (Map.Entry<String, String> entry : _user_open.entrySet()) {
			System.out.println(entry);
			if (entry.getValue().equals(docname)
					&& !entry.getKey().equals(editor)) {
				// skip if user is the editer
				try {
					Transceiver client = new NettyTransceiver(
							new InetSocketAddress(
									_user_host.get(entry.getKey()),
									_user_port.get(entry.getKey())));
					MSClient proxy = (MSClient) SpecificRequestor.getClient(
							MSClient.class, client);
					proxy.UpdateDocument(content);
					proxy.UpdateEdit(editor);
					client.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	} 

	@Override
	public boolean editDocumentInsert(CharSequence username, CharSequence text,
			int position) throws AvroRemoteException {
		/*
		 * insertText(user, text, position) Insert Text into a document openend
		 * by the user if no suitable document is found, return false
		 */
		if (!_user_edit.containsValue(username.toString()))
			return false;
		else {
			String docname = "";
			for (Map.Entry<String, String> entry : _user_edit.entrySet()) {
				if (entry.getValue().equals(username.toString())) {
					docname = entry.getKey();
				}
			}

			String content = _doc_content.get(docname);

			if (position == 0) {
				content = text + content;
			} else if (position >= content.length()) {
				content = content + text;
			} else {
				content = content.substring(0, position) + text
						+ content.substring(position);
			}
			_doc_content.put(docname, content);

			updateViews(docname, username.toString(), content);

			return true;
		}
	}

	@Override
	public boolean editDocumentDelete(CharSequence username, int begin, int end)
			throws AvroRemoteException {
		/*
		 * deleteText(user, begin, end) Delete all text in between the
		 * boundaries if no suitable document is found, return false
		 */

		if (!_user_edit.containsValue(username.toString()))
			return false;
		else {
			String docname = "";
			for (Map.Entry<String, String> entry : _user_edit.entrySet()) {
				if (entry.getValue().equals(username.toString())) {
					docname = entry.getKey();
				}
			}
			String content = _doc_content.get(docname).toString();
			if (begin == 0) {
				content = content.substring(end);
			} else if (end == content.length()) {
				content = content.substring(0, begin);
			} else {
				content = content.substring(0, begin) + content.substring(end);
			}
			_doc_content.put(docname, content);

			updateViews(docname, username.toString(), content);
			return true;
		}
	}

	@Override
	public boolean stopEditingDocument(CharSequence user)
			throws AvroRemoteException {
		if (!_user_edit.containsValue(user.toString()))
			return false;
		else {
			String docname = "";
			for (Map.Entry<String, String> entry : _user_edit.entrySet()) {
				if (entry.getValue().equals(user.toString())) {
					docname = entry.getKey();
				}
			}
			_user_edit.put(docname, "<NONE>".toString());
			updateEditors(docname, "<NONE>");

		}
		return true;
	}

	@Override
	public CharSequence getDocumentEditor(CharSequence document)
			throws AvroRemoteException {
		return _user_edit.get(document.toString());
	}

	@Override
	public void run() {

		for (Map.Entry<String, String> entry : _doc_content.entrySet()) {
			// write all files back to the master
			try {
				_m_proxy.slaveWriteBack(entry.getKey(), entry.getValue());
			} catch (AvroRemoteException e) {
				e.printStackTrace();
			}
		}

	}

	public boolean logoutUser(CharSequence user) throws AvroRemoteException {
		System.out.println("logoutUser()");
		if (_user_edit.containsValue(user.toString()))
			stopEditingDocument(user);

		if (_user_open.containsKey(user.toString())){
			String docname = _user_open.get(user.toString());
			removeUserFromDocument(user, docname);
		}
			

		return true;
	}

	@Override
	public Map<CharSequence, CharSequence> masterShutdown()
			throws AvroRemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CharSequence getDocumentTextByUser(CharSequence user)
			throws AvroRemoteException {
		String docname = _user_open.get(user.toString());
		return _doc_content.get(docname.toString());
	}

	@Override
	public CharSequence getDocumentTextByName(CharSequence document)
			throws AvroRemoteException {
		return _doc_content.get(document.toString());
	}

	@Override
	public boolean removeUserFromDocument(CharSequence user,
			CharSequence document) throws AvroRemoteException {
		if(_user_edit.containsValue(user.toString())) stopEditingDocument(user);
		if (document.toString().equals(_user_open.get(user.toString()))) {
			_user_open.remove(user.toString());
			_user_port.remove(user.toString());
			_user_host.remove(user.toString());
			return true;
		}
		return false;
	}

	@Override
	public boolean addDocument(CharSequence document, CharSequence content)
			throws AvroRemoteException {
		_doc_content.put(document.toString(), content.toString());
		_user_edit.put(document.toString(), "<NONE>".toString());
		return true;
	}

	@Override
	public CharSequence deleteDocument(CharSequence document)
			throws AvroRemoteException {
		String content = _doc_content.get(document.toString());
		_doc_content.remove(document);
		return content;
	}

}
