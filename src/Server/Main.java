package Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Scanner;

import org.apache.avro.ipc.NettyServer;
import org.apache.avro.ipc.NettyTransceiver;
import org.apache.avro.ipc.Server;
import org.apache.avro.ipc.specific.SpecificRequestor;
import org.apache.avro.ipc.specific.SpecificResponder;

import docedit.avro.SharedDocs;

public class Main {
	//starts up server at a port might, not suitable for containing all data.
	public static void main(String[] args) {
		try {
			startServer();
			
	        //testCreate(); // test creation of file	-> false
	        //testRegister(); // test registration of user -> true
	        //testRegister(); // test registration of user -> false
			//while(true){}
			Scanner reader = new Scanner(System.in);
			System.out.println("Enter the first number");
			//get user input for a
			reader.nextInt();
			

			//testLogin();
	        //testView();
			//testGetDoc();
			//testGetDocInfo();
			//testUnshare();
			//testShare();
	        //testEdit();
			//testClose();
	        stopServer();
	        

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static Server server;
	
    private static void startServer() throws IOException {
        server = new NettyServer(new SpecificResponder(SharedDocs.class, new SharedDocsImpl()), new InetSocketAddress(65111));
        System.out.println("Started Server");
        // the server implements the SharedDocs protocol (SharedDocsImpl)
    }
    
    private static void stopServer() throws IOException {
        server.close();
        System.out.println("Closed Server");
        // the server implements the SharedDocs protocol (SharedDocsImpl)
    }
    
    @SuppressWarnings("unused")
	private static void testCreate() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        boolean result = proxy.create("admin", "test2.txt");
        if( result )System.out.println("Succeeded to write file");
        else System.out.println("Failed to write file");
        client.close();
    }
    @SuppressWarnings("unused")
	private static void testRegister() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        boolean result = proxy.register("admin4", "root");
        result = proxy.register("admin5", "root");
        if( result )System.out.println("Succeeded to register user");
        else System.out.println("Failed to register user");
        
        client.close();
    }  
    @SuppressWarnings("unused")
	private static void testLogin() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        boolean result = proxy.login("admin", "root", 0);
        if( result )System.out.println("Succeeded to login user");
        else System.out.println("Failed to login user");
        client.close();
    }
    @SuppressWarnings("unused")
	private static void testView() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        CharSequence result = proxy.view("test3.txt", "admin1");
        System.out.println(result);
        client.close();
    }   
    @SuppressWarnings("unused")
	private static void testGetDoc() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        CharSequence result = proxy.getDocs("admin");
        
        System.out.println(result);
        client.close();
    }    
    @SuppressWarnings("unused")
	private static void testGetDocInfo() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        CharSequence result = proxy.getDocInfo("test3.txt");
        
        System.out.println(result);
        client.close();
    }    
    @SuppressWarnings("unused")
	private static void testUnshare() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        boolean result = proxy.unshare("admin1","test3.txt");
        result = proxy.unshare("admin3","test3.txt");
        result = proxy.unshare("admin4","test3.txt");
        result = proxy.unshare("admin5","test3.txt");
        
        if( result )System.out.println("Succeeded to unshare document");
        else System.out.println("Failed to unshare document");
        client.close();
    }    
    @SuppressWarnings("unused")
	private static void testShare() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        boolean result = proxy.share("admin2","test3.txt");
        
        if( result )System.out.println("Succeeded to unshare document");
        else System.out.println("Failed to unshare document");
        client.close();
    }
	private static void testEdit() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        CharSequence result = proxy.view("admin2","test3.txt");
        boolean test = proxy.edit("admin2");
        System.out.println(proxy.getEditor("test3.txt"));
        client.close();
    } 
	
	private static void testClose() throws IOException{
        NettyTransceiver client = new NettyTransceiver(new InetSocketAddress(65111));
        SharedDocs proxy = (SharedDocs) SpecificRequestor.getClient(SharedDocs.class, client);
        System.out.println("Client built, got proxy");
    	
        boolean result = proxy.close("admin1");
        client.close();
    } 

}
