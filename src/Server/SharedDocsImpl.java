package Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Scanner;

import org.apache.avro.AvroRemoteException;
import org.apache.avro.ipc.NettyServer;
import org.apache.avro.ipc.Server;
import org.apache.avro.ipc.specific.SpecificResponder;

import docedit.avro.SharedDocs;

//implement all function in here might require extra class for storage of data entities.
public class SharedDocsImpl implements SharedDocs {
	
	
	public static void main(String[] args) {
		try {
		startServer();
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter the first number");
		//get user input for a
		reader.nextInt();
		
		reader.close();
        stopServer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// necessary code
	private static Server server;
	
    private static void startServer() throws IOException {
        server = new NettyServer(new SpecificResponder(SharedDocs.class, new SharedDocsImpl()), new InetSocketAddress(65111));
        System.out.println("Started Server");
        // the server implements the SharedDocs protocol (SharedDocsImpl)
    }
    
    private static void stopServer() throws IOException {
        server.close();
        System.out.println("Closed Server");
        // the server implements the SharedDocs protocol (SharedDocsImpl)
    }

	@Override
	public boolean create(CharSequence user, CharSequence docName) throws AvroRemoteException {
		if (new File(docName.toString()).exists())
			return false;
		else {
			try {
				File f = new File("Documents/"+docName.toString());
				f.createNewFile();
				f = new File("Users/Rights.txt");
				Writer writer = new BufferedWriter(new FileWriter(f, true));
				writer.append(user.toString() + "$$$" + docName.toString()+ "$$$OWNER");
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	@Override
	public CharSequence view(CharSequence docname, CharSequence username) throws AvroRemoteException {
		try {
		File f = new File("Documents/"+docname); 
		BufferedReader br = new BufferedReader(new FileReader(f));
		
		File v = new File("Users/Viewing.txt");
		Writer pw = new BufferedWriter(new FileWriter(v, true));
		pw.append(username.toString() + "$$$" + docname.toString()+"\n");
		pw.close();
		
		String line;
		String document = "";
		CharSequence rv;

		while ((line = br.readLine()) != null) {
				document += line;
				document += "\n";
		}
		br.close();
		document = document.substring(0, document.length()-1); //remove last enter from file
		rv = document;
		return rv;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean edit(CharSequence user) throws AvroRemoteException {
		//put(getView(user.toString()), user.toString());
		
		try {
			String docname = getView(user.toString());
			// check no one else is editing
			CharSequence ed = getEditor(docname);
			String es = ed.toString();
			if(!es.equals("")){
				return false;
			}else{
				File f = new File("Users/Editing.txt");
				Writer pw = new BufferedWriter(new FileWriter(f, true));
				pw.append(user.toString() + "$$$" + docname + "\n");
				pw.close();
				return true;
			}

			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean share(CharSequence user, CharSequence docName)
			throws AvroRemoteException {
		//check if user exists
		try {
			File f = new File("Users/Users.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString())){
					File r = new File("Users/Rights.txt");
					Writer pw = new BufferedWriter(new FileWriter(r, true));
					pw.append(user.toString() + "$$$" + docName.toString()+ "$$$SHARED\n");
					pw.close();
					break;
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public int login(CharSequence user, CharSequence password)
			throws AvroRemoteException {
		//needs a login proxy for the client, used for push messages
		try {
			File f = new File("Users/Users.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString())
						&& sline[1].equals(password.toString())){
					br.close();
					// add user to proxy map
					
					return 1;
					
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return 0;
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
	}

	@Override
	public boolean logout(CharSequence user) throws AvroRemoteException {
		// remove users proxy from login map
		return false;
	}

	@Override
	public boolean register(CharSequence user, CharSequence password)
			throws AvroRemoteException {
		try {
			File f = new File("Users/Users.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString())){
					br.close();
					return false;
				}
					
			}
			br.close();
			Writer writer = new BufferedWriter(new FileWriter(f, true));
			writer.append(user.toString() + "$$$" + password.toString()+"\n");
			writer.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public CharSequence getDocs(CharSequence user) throws AvroRemoteException {
		String documents="";
		try {
			File f = new File("Users/Rights.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString())){
					documents+=sline[1];
					documents+="$$$";
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		CharSequence rv = documents;
		return rv;
	}

	@Override
	public boolean unshare(CharSequence user, CharSequence docName)
			throws AvroRemoteException {
		boolean succ = false;
		try {
			File f = new File("Users/Rights.txt");
			File tempf = new File(f.getAbsolutePath() + ".tmp");
			BufferedReader br = new BufferedReader(new FileReader(f));
			PrintWriter pw = new PrintWriter(new FileWriter(tempf));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if ((sline[0].equals(user.toString())) 
						&& (sline[1].equals(docName.toString()))
						&& !sline[2].equals("OWNER")){
						succ = true;
				}else{
					pw.println(line);
			        pw.flush();
				}
			}
			br.close();
			pw.close();
			//Delete the original file
		      if (!f.delete()) {
		        System.out.println("Could not delete file");
		        return false;
		      }

		      //Rename the new file to the filename the original file had.
		     if (!tempf.renameTo(f)) System.out.println("Could not rename file");
		     
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
			
		return succ;
	}


	@Override
	public CharSequence getDocInfo(CharSequence document)
			throws AvroRemoteException {
		String rv = "";
		try {
			File f = new File("Users/Rights.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[1].equals(document.toString()) ){
					if(!sline[2].equals("OWNER")){
						rv += sline[0].toString()+"$$$";
					}
					else rv = sline[0].toString()+"$$$"+rv;
				}
			}
			br.close();
		
		CharSequence rc = rv;
		return rc;
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public CharSequence getEditor(CharSequence document)
			throws AvroRemoteException {
		try {
			String rv="";
			File f = new File("Users/Editing.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[1].equals(document.toString()) ){
					rv = sline[0];
					break;
				}
			}
			br.close();
			CharSequence rc = rv;
			return rc;
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

	}
	
	public String getView(String user){
		try {
			File f = new File("Users/Viewing.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString()) ){
					br.close();
					return sline[1];
				}
			}
			br.close();
		
		return "";
		
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public boolean close(CharSequence user) throws AvroRemoteException {
		boolean succ = false;
		try {
			File f = new File("Users/Viewing.txt");
			File tempf = new File(f.getAbsolutePath() + ".tmp");
			BufferedReader br = new BufferedReader(new FileReader(f));
			PrintWriter pw = new PrintWriter(new FileWriter(tempf));
			
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString())){
						succ = true;
				}else{
					pw.println(line);
			        pw.flush();
				}
			}
			br.close();
			pw.close();
			//Delete the original file
		      if (!f.delete()) {
		        System.out.println("Could not delete file");
		        return false;
		      }

		      //Rename the new file to the filename the original file had.
		     if (!tempf.renameTo(f)) System.out.println("Could not rename file");
		     
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
			
		return succ;
	}

	@Override
	public boolean insertText(CharSequence username, CharSequence text, int position)
			throws AvroRemoteException {
		//System.out.println(username.toString()+" adds "+ text.toString()+ " at "+ Integer.toString(position));
		try {
			
			String Text = text.toString();
			int ascii = (int) Text.charAt(0);
			if (ascii == 13){
				Text = "\n";
			}
			
			//get document name
			String docname="";
			File f = new File("Users/Editing.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(username.toString())){
						docname=sline[1];
						break;
				}
			}
			br.close();
			
			//System.out.println(docname);
			
			File edit_f = new File("Documents/"+docname);
			
			BufferedReader bv = new BufferedReader(new FileReader(edit_f));
						
			String doc="";
			
			while ((line = bv.readLine()) != null) {
				//System.out.println("adding line");
				//System.out.println(line);
				doc+= line+"\n";
			}
			doc = doc.substring(0, doc.length()-1); //remove last enter from file
			//System.out.println("read from document");
			//System.out.println(doc);
			
			if (position==0){
				doc = Text+doc;
			}else if (position >= doc.length()){
				doc = doc + Text;
			}else{
				doc = doc.substring(0, position)+ Text+ doc.substring(position);
			}
			//System.out.println("writing to document");
			//System.out.println(doc);
			
			PrintWriter pw = new PrintWriter(new FileWriter(edit_f));
			pw.write(doc);
			pw.flush();
			bv.close();
			pw.close();
		     
		     
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (ArithmeticException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
			
		return true;
	}

	@Override
	public boolean deleteText(CharSequence username, int begin, int end) throws AvroRemoteException {
		try {
			
			//get document name
			String docname="";
			File f = new File("Users/Editing.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(username.toString())){
						docname=sline[1];
						break;
				}
			}
			br.close();
			
			//System.out.println(docname);
			
			File edit_f = new File("Documents/"+docname);
			
			BufferedReader bv = new BufferedReader(new FileReader(edit_f));
						
			String doc="";
			
			while ((line = bv.readLine()) != null) {
				//System.out.println("adding line");
				//System.out.println(line);
				doc+= line+"\n";
			}
			//System.out.println("read from document");
			//System.out.println(doc);
			doc = doc.substring(0, doc.length()-1);
			if(begin==0){
				doc = doc.substring(end);
			}
			else if(end == doc.length()){
				doc = doc.substring(0, begin);
			}
			else{
				doc = doc.substring(0, begin) + doc.substring(end);
			}
			
			//System.out.println("writing to document");
			//System.out.println(doc);
			
			PrintWriter pw = new PrintWriter(new FileWriter(edit_f));
			pw.write(doc);
			pw.flush();
			bv.close();
			pw.close();
		     
		     
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (ArithmeticException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
			
		return true;
	}

	@Override
	public boolean stopedit(CharSequence user) throws AvroRemoteException {
		boolean succ = false;
		try {
			File f = new File("Users/Editing.txt");
			File tempf = new File(f.getAbsolutePath() + ".tmp");
			BufferedReader br = new BufferedReader(new FileReader(f));
			PrintWriter pw = new PrintWriter(new FileWriter(tempf));
			
			String line;
			while ((line = br.readLine()) != null) {
				String[] sline = line.split("\\$\\$\\$");
				if (sline[0].equals(user.toString())){
						succ = true;
				}else{
					pw.println(line);
			        pw.flush();
				}
			}
			br.close();
			pw.close();
			//Delete the original file
		      if (!f.delete()) {
		        System.out.println("Could not delete file");
		        return false;
		      }

		      //Rename the new file to the filename the original file had.
		     if (!tempf.renameTo(f)) System.out.println("Could not rename file");
		     
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
			
		return succ;
	}

}