/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package docedit.avro;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public interface SharedDocs {
  public static final org.apache.avro.Protocol PROTOCOL = org.apache.avro.Protocol.parse("{\"protocol\":\"SharedDocs\",\"namespace\":\"docedit.avro\",\"types\":[],\"messages\":{\"create\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"},{\"name\":\"document\",\"type\":\"string\"}],\"response\":\"boolean\"},\"view\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"},{\"name\":\"document\",\"type\":\"string\"}],\"response\":\"string\"},\"edit\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"}],\"response\":\"boolean\"},\"stopedit\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"}],\"response\":\"boolean\"},\"share\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"},{\"name\":\"document\",\"type\":\"string\"}],\"response\":\"boolean\"},\"close\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"}],\"response\":\"boolean\"},\"login\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"},{\"name\":\"password\",\"type\":\"string\"}],\"response\":\"int\"},\"logout\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"}],\"response\":\"boolean\"},\"register\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"},{\"name\":\"password\",\"type\":\"string\"}],\"response\":\"boolean\"},\"getDocs\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"}],\"response\":\"string\"},\"getDocInfo\":{\"request\":[{\"name\":\"document\",\"type\":\"string\"}],\"response\":\"string\"},\"getEditor\":{\"request\":[{\"name\":\"document\",\"type\":\"string\"}],\"response\":\"string\"},\"unshare\":{\"request\":[{\"name\":\"user\",\"type\":\"string\"},{\"name\":\"document\",\"type\":\"string\"}],\"response\":\"boolean\"},\"insertText\":{\"request\":[{\"name\":\"username\",\"type\":\"string\"},{\"name\":\"text\",\"type\":\"string\"},{\"name\":\"position\",\"type\":\"int\"}],\"response\":\"boolean\"},\"deleteText\":{\"request\":[{\"name\":\"username\",\"type\":\"string\"},{\"name\":\"begin\",\"type\":\"int\"},{\"name\":\"end\",\"type\":\"int\"}],\"response\":\"boolean\"}}}");
  boolean create(java.lang.CharSequence user, java.lang.CharSequence document) throws org.apache.avro.AvroRemoteException;
  java.lang.CharSequence view(java.lang.CharSequence user, java.lang.CharSequence document) throws org.apache.avro.AvroRemoteException;
  boolean edit(java.lang.CharSequence user) throws org.apache.avro.AvroRemoteException;
  boolean stopedit(java.lang.CharSequence user) throws org.apache.avro.AvroRemoteException;
  boolean share(java.lang.CharSequence user, java.lang.CharSequence document) throws org.apache.avro.AvroRemoteException;
  boolean close(java.lang.CharSequence user) throws org.apache.avro.AvroRemoteException;
  int login(java.lang.CharSequence user, java.lang.CharSequence password) throws org.apache.avro.AvroRemoteException;
  boolean logout(java.lang.CharSequence user) throws org.apache.avro.AvroRemoteException;
  boolean register(java.lang.CharSequence user, java.lang.CharSequence password) throws org.apache.avro.AvroRemoteException;
  java.lang.CharSequence getDocs(java.lang.CharSequence user) throws org.apache.avro.AvroRemoteException;
  java.lang.CharSequence getDocInfo(java.lang.CharSequence document) throws org.apache.avro.AvroRemoteException;
  java.lang.CharSequence getEditor(java.lang.CharSequence document) throws org.apache.avro.AvroRemoteException;
  boolean unshare(java.lang.CharSequence user, java.lang.CharSequence document) throws org.apache.avro.AvroRemoteException;
  boolean insertText(java.lang.CharSequence username, java.lang.CharSequence text, int position) throws org.apache.avro.AvroRemoteException;
  boolean deleteText(java.lang.CharSequence username, int begin, int end) throws org.apache.avro.AvroRemoteException;

  @SuppressWarnings("all")
  public interface Callback extends SharedDocs {
    public static final org.apache.avro.Protocol PROTOCOL = docedit.avro.SharedDocs.PROTOCOL;
    void create(java.lang.CharSequence user, java.lang.CharSequence document, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void view(java.lang.CharSequence user, java.lang.CharSequence document, org.apache.avro.ipc.Callback<java.lang.CharSequence> callback) throws java.io.IOException;
    void edit(java.lang.CharSequence user, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void stopedit(java.lang.CharSequence user, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void share(java.lang.CharSequence user, java.lang.CharSequence document, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void close(java.lang.CharSequence user, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void login(java.lang.CharSequence user, java.lang.CharSequence password, org.apache.avro.ipc.Callback<java.lang.Integer> callback) throws java.io.IOException;
    void logout(java.lang.CharSequence user, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void register(java.lang.CharSequence user, java.lang.CharSequence password, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void getDocs(java.lang.CharSequence user, org.apache.avro.ipc.Callback<java.lang.CharSequence> callback) throws java.io.IOException;
    void getDocInfo(java.lang.CharSequence document, org.apache.avro.ipc.Callback<java.lang.CharSequence> callback) throws java.io.IOException;
    void getEditor(java.lang.CharSequence document, org.apache.avro.ipc.Callback<java.lang.CharSequence> callback) throws java.io.IOException;
    void unshare(java.lang.CharSequence user, java.lang.CharSequence document, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void insertText(java.lang.CharSequence username, java.lang.CharSequence text, int position, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
    void deleteText(java.lang.CharSequence username, int begin, int end, org.apache.avro.ipc.Callback<java.lang.Boolean> callback) throws java.io.IOException;
  }
}